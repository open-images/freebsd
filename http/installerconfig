DISTRIBUTIONS="base.txz kernel.txz"

if [ `uname -m` = "amd64" ]; then
    DISTRIBUTIONS="${DISTRIBUTIONS} lib32.txz"
fi

if [ -e /dev/vtbd0 ]; then
	GEOM=vtbd0 # VirtIO (QEMU)
fi

if [ -z $GEOM ]; then
	echo "ERROR: No disks found." >&2
	exit 1
fi

# Workaround for https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=203777
export nonInteractive="YES"
export ZFSBOOT_DISKS="$GEOM"
export ZFSBOOT_CONFIRM_LAYOUT=0

#!/bin/sh -x

ifdev=$(ifconfig | grep '^[a-z]' | cut -d: -f1 | head -n 1)
# Enable required services
cat >> /etc/rc.conf << EOT
ifconfig_${ifdev}="DHCP"
ifconfig_${ifdev}_ipv6="DHCP"
sshd_enable="YES"
EOT

# Tune and boot from zfs
cat >> /boot/loader.conf << EOT
vm.kmem_size="200M"
vm.kmem_size_max="200M"
vfs.zfs.arc_max="40M"
vfs.zfs.vdev.cache.size="5M"
kern.geom.part.auto_resize="1"
autoboot_delay=3
EOT

# zfs doesn't use an fstab, but some rc scripts expect one
touch /etc/fstab


# Use DHCP to get the network configuration
sysrc ifconfig_DEFAULT=SYNCDHCP

# Enable sshd by default
sysrc -f "$SSHD_RC_CONF_FILE" sshd_enable=YES
# Disable DNS lookups by default to make SSH connect quickly
sed -i '' -e 's/^#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config
# Allow root logins during build.  Deactivated upon cleanup
sed -i '' -e 's/^#PermitRootLogin no/PermitRootLogin yes/' /etc/ssh/sshd_config


# Change root's password
echo 'super_admin' | pw usermod root -h 0


# Set up user accounts
echo "freebsd" | pw -V /etc useradd freebsd -h 0 -s /bin/sh -G wheel -d /usr/home/freebsd -c "FreeBSD User"
echo "freebsd" | pw -V /etc usermod root

mkdir -p /usr/home/freebsd
chown 1001:1001 /usr/home/freebsd
ln -s /usr/home /home

# Reboot quickly, don't wait at the panic screen
{
	echo 'debug.trace_on_panic=1'
	echo 'debug.debugger_on_panic=0'
	echo 'kern.panic_reboot_wait_time=0'
} >> "$SYSCTL_CONF_FILE"

# The console is not interactive, so we might as well boot quickly
sysrc -f /boot/loader.conf autoboot_delay=-1

# Reboot
shutdown -r now
