#!/bin/sh
set -e

pkg install -qy qemu-guest-agent
sysrc qemu_guest_agent_enable=YES
sysrc qemu_guest_agent_flags="-d -v -l /var/log/qemu-ga.log"
