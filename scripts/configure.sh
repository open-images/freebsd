#!/bin/sh -eux

# Set the time correctly
ntpdate -v -b 0.pool.ntp.org || true

# Install curl and ca_root_nss
pkg install -y curl ca_root_nss dmidecode net/dhcpcd;

# Emulate the ETCSYMLINK behavior of ca_root_nss; this is for FreeBSD 10,
# where fetch(1) was massively refactored and doesn't come with
# SSL CAcerts anymore
ln -sf /usr/local/share/certs/ca-root-nss.crt /etc/ssl/cert.pem;

# Avoid pausing at the boot screen
cat >>/boot/loader.conf << LOADER_CONF
beastie_disable="YES"
loader_logo="none"
hw.memtest.tests="0"
console="comconsole,vidconsole"
comconsole_speed="115200"
autoboot_delay=3
LOADER_CONF

# Configure DHCPCD client to request DHCPv6 IA_NA and send the hostname
# during the request
cat >>/usr/local/etc/dhcpcd.conf << DHCP_CONF
hostname
ia_na 1
DHCP_CONF
# Do not generate stable private ipv6 address based on the DUID
sed -i '' -e 's/^slaac private/#slaac private/' /usr/local/etc/dhcpcd.conf

# As sharedfolders are not in defaults ports tree, we will use NFS sharing
cat >>/etc/rc.conf << RC_CONF
dhclient_program="/usr/local/sbin/dhcpcd"
rpcbind_enable="YES"
nfs_server_enable="YES"
mountd_flags="-r"
RC_CONF

echo 'Disable X11 in make.conf because Vagrants VMs are (usually) headless'
cat >>/etc/make.conf << MAKE_CONF
WITHOUT_X11="YES"
WITHOUT_GUI="YES"
MAKE_CONF

echo 'Update the locate DB'
/etc/periodic/weekly/310.locate
