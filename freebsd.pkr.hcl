packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
  }
}

source "qemu" "freebsd" {
  boot_wait = "5s"
  boot_command = [
    "<esc><wait>",
    "<wait10s>",
    "boot -s<enter>",
    "<wait15s>",
    "/bin/sh<enter><wait>",
    "mdmfs -s 500m md /tmp<enter><wait>", # Create
    "dhclient -l /tmp/dhclient.lease.vtnet0 vtnet0<enter><wait5>", # Get IP from DHCP
    "fetch -o /tmp/installerconfig http://{{ .HTTPIP }}:{{ .HTTPPort }}/installerconfig<enter><wait5>",
    "FILESYSTEM=${var.filesystem}<enter>", # retrieve config form http sever of packer
    "export FILESYSTEM<enter>",
    "ZFS_COMPRESSION=${var.zfs_compression}<enter>",
    "export ZFS_COMPRESSION<enter>",
    "RC_CONF_FILE=${var.rc_conf_file}<enter>",
    "export RC_CONF_FILE<enter>",
    "bsdinstall script /tmp/installerconfig<enter>"
  ]
  shutdown_command = "poweroff"

  disk_size = "8192M"
  disk_compression = true
  cpus      = "${var.cpus}"
  memory    = "${var.memory}"
  http_directory   = "http"

  iso_checksum = "file:${var.mirror}/${var.directory}/ISO-IMAGES/${var.revision}/CHECKSUM.SHA256-FreeBSD-${var.revision}-${var.branch}-${var.arch}${var.build_date}${var.git_commit}"
  iso_urls     = [
    "iso/FreeBSD-${var.revision}-${var.branch}-${var.arch}${var.build_date}${var.git_commit}-disc1.iso",
    "${var.mirror}/${var.directory}/ISO-IMAGES/${var.revision}/FreeBSD-${var.revision}-${var.branch}-${var.arch}${var.build_date}${var.git_commit}-disc1.iso"
  ]

  ssh_port     = 22
  ssh_username = "root"
  ssh_password = "super_admin"
  ssh_timeout  = "100s"

  vm_name = "freebsd.qcow2"
  format  = "qcow2"
  headless = true
}

build {
  sources = [
    "source.qemu.freebsd",
  ]

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    scripts         = [
      "scripts/update.sh",
      "scripts/vmtools.sh",
      "scripts/configure.sh",
      "scripts/sudoers.sh",
      "scripts/cloud-init.sh",
      "scripts/cleanup.sh"
    ]
  }
}
